/*
 Navicat MySQL Data Transfer

 Source Server         : 本地MySql
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : localhost:3306
 Source Schema         : videowebsite

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 29/02/2020 15:28:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ban_keyword
-- ----------------------------
DROP TABLE IF EXISTS `ban_keyword`;
CREATE TABLE `ban_keyword`  (
  `ban_keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_keyword_userid` int(11) NOT NULL,
  `ban_keyword_string` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ban_keyword_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ban_keyword
-- ----------------------------
INSERT INTO `ban_keyword` VALUES (21, 2, '呵呵');
INSERT INTO `ban_keyword` VALUES (24, 1, '大圣');
INSERT INTO `ban_keyword` VALUES (25, 1, 'sss');

-- ----------------------------
-- Table structure for complaint
-- ----------------------------
DROP TABLE IF EXISTS `complaint`;
CREATE TABLE `complaint`  (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_danmaku_id` int(11) NOT NULL,
  `complaint_user_id` int(11) NOT NULL,
  `complaint_type` int(255) NULL DEFAULT NULL,
  `complaint_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `complaint_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`complaint_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of complaint
-- ----------------------------
INSERT INTO `complaint` VALUES (23, 33, 2, 5, '他骂我', 1);
INSERT INTO `complaint` VALUES (24, 15, 2, 12, '', 1);
INSERT INTO `complaint` VALUES (25, 13, 2, 12, '', 1);
INSERT INTO `complaint` VALUES (35, 50, 2, 12, '', 1);

-- ----------------------------
-- Table structure for complainttype
-- ----------------------------
DROP TABLE IF EXISTS `complainttype`;
CREATE TABLE `complainttype`  (
  `complainttype_id` int(11) NOT NULL AUTO_INCREMENT,
  `complainttype_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`complainttype_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of complainttype
-- ----------------------------
INSERT INTO `complainttype` VALUES (1, '违法违禁');
INSERT INTO `complainttype` VALUES (2, '色情低俗');
INSERT INTO `complainttype` VALUES (3, '恶意刷屏');
INSERT INTO `complainttype` VALUES (4, '赌博诈骗');
INSERT INTO `complainttype` VALUES (5, '人身攻击');
INSERT INTO `complainttype` VALUES (6, '侵犯隐私');
INSERT INTO `complainttype` VALUES (7, '垃圾广告');
INSERT INTO `complainttype` VALUES (8, '视频无关');
INSERT INTO `complainttype` VALUES (9, '引战');
INSERT INTO `complainttype` VALUES (10, '剧透');
INSERT INTO `complainttype` VALUES (11, '青少年不良信息');
INSERT INTO `complainttype` VALUES (12, '其它');

-- ----------------------------
-- Table structure for danmaku
-- ----------------------------
DROP TABLE IF EXISTS `danmaku`;
CREATE TABLE `danmaku`  (
  `danmaku_id` int(11) NOT NULL AUTO_INCREMENT,
  `danmaku_videoid` int(11) NOT NULL,
  `danmaku_videotime` int(11) NOT NULL,
  `danmaku_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `danmaku_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danmaku_type` int(11) NULL DEFAULT NULL,
  `danmaku_fontsize` int(11) NULL DEFAULT NULL,
  `danmaku_senderid` int(11) NOT NULL,
  `danmaku_sendtime` datetime(0) NOT NULL,
  `danmaku_status` int(11) NOT NULL,
  PRIMARY KEY (`danmaku_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of danmaku
-- ----------------------------
INSERT INTO `danmaku` VALUES (1, 1, 32, '好好好', '#000000', 0, 1, 1, '2020-01-23 02:14:04', 0);
INSERT INTO `danmaku` VALUES (12, 1, 50, '阿斯顿', '#000000', 1, 1, 1, '2020-01-28 13:46:23', 2);
INSERT INTO `danmaku` VALUES (13, 1, 9, '阿斯顿', '#000000', 1, 1, 1, '2020-01-28 13:54:46', 0);
INSERT INTO `danmaku` VALUES (14, 1, 109, '热发电', '#000000', 1, 1, 1, '2020-01-28 13:54:58', 0);
INSERT INTO `danmaku` VALUES (15, 1, 50, '啊额人', '#000000', 1, 1, 1, '2020-01-28 13:54:58', 3);
INSERT INTO `danmaku` VALUES (16, 1, 50, '文认为人阿', '#000000', 1, 1, 1, '2020-01-28 13:54:58', 0);
INSERT INTO `danmaku` VALUES (17, 1, 50, '热热我热', '#000000', 1, 1, 1, '2020-01-28 13:54:58', 1);
INSERT INTO `danmaku` VALUES (19, 1, 50, '大赛分为', '#000000', 1, 1, 1, '2020-01-29 13:46:49', 1);
INSERT INTO `danmaku` VALUES (24, 1, 50, '萨达', '#000000', 1, 1, 1, '2020-01-29 14:13:02', 1);
INSERT INTO `danmaku` VALUES (25, 1, 50, '阿斯顿', '#000000', 1, 1, 1, '2020-01-29 14:13:07', 1);
INSERT INTO `danmaku` VALUES (26, 1, 50, '萨达', '#000000', 1, 1, 1, '2020-01-29 14:13:09', 1);
INSERT INTO `danmaku` VALUES (27, 1, 50, '萨达aa', '#000000', 1, 1, 1, '2020-01-29 14:13:10', 0);
INSERT INTO `danmaku` VALUES (28, 1, 50, '洒', '#000000', 1, 1, 1, '2020-01-29 14:13:12', 0);
INSERT INTO `danmaku` VALUES (29, 1, 50, '大圣', '#000000', 1, 1, 1, '2020-01-29 14:18:47', 0);
INSERT INTO `danmaku` VALUES (30, 1, 50, 'VS的', '#000000', 1, 1, 1, '2020-01-29 14:19:36', 0);
INSERT INTO `danmaku` VALUES (31, 1, 50, '倒萨', '#000000', 1, 1, 1, '2020-01-29 14:19:58', 0);
INSERT INTO `danmaku` VALUES (32, 1, 50, '阿斯顿是', '#000000', 1, 1, 1, '2020-01-30 12:20:33', 2);
INSERT INTO `danmaku` VALUES (33, 2, 50, '草泥马', '#000000', 1, 1, 1, '2020-02-20 21:07:43', 0);
INSERT INTO `danmaku` VALUES (34, 3, 50, '呵呵呵呵呵', '#000000', 1, 1, 1, '2020-02-22 21:25:17', 2);
INSERT INTO `danmaku` VALUES (35, 3, 50, '那是真的牛逼', '#000000', 1, 1, 1, '2020-02-22 21:25:23', 1);
INSERT INTO `danmaku` VALUES (36, 1, 30, '大圣', '#000000', 1, 1, 1, '2020-02-22 21:29:20', 0);
INSERT INTO `danmaku` VALUES (37, 1, 30, '萨达', '#000000', 1, 1, 1, '2020-02-22 21:30:09', 0);
INSERT INTO `danmaku` VALUES (38, 1, 30, '萨达吖', '#000000', 1, 1, 1, '2020-02-22 21:32:21', 0);
INSERT INTO `danmaku` VALUES (39, 1, 30, '萨达', '#000000', 1, 1, 1, '2020-02-22 21:32:40', 0);
INSERT INTO `danmaku` VALUES (40, 1, 30, '在吗', '#000000', 1, 1, 1, '2020-02-22 21:34:03', 2);
INSERT INTO `danmaku` VALUES (41, 3, 30, '阿迪斯', '#000000', 1, 1, 1, '2020-02-22 21:36:17', 2);
INSERT INTO `danmaku` VALUES (42, 4, 30, '这就是你分手的借口', '#000000', 0, 1, 1, '2020-02-23 23:38:41', 0);
INSERT INTO `danmaku` VALUES (43, 3, 30, '?', '#000000', 0, 1, 1, '2020-02-24 01:20:34', 2);
INSERT INTO `danmaku` VALUES (44, 3, 30, '???', '#000000', 0, 1, 1, '2020-02-24 01:21:19', 0);
INSERT INTO `danmaku` VALUES (45, 3, 30, '好起来了', '#000000', 0, 1, 1, '2020-02-24 01:24:29', 2);
INSERT INTO `danmaku` VALUES (46, 4, 30, '?', '#000000', 0, 1, 1, '2020-02-25 13:49:41', 0);
INSERT INTO `danmaku` VALUES (47, 1, 30, '????', '#000000', 0, 1, 1, '2020-02-26 07:20:22', 2);
INSERT INTO `danmaku` VALUES (48, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:47', 0);
INSERT INTO `danmaku` VALUES (49, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:48', 0);
INSERT INTO `danmaku` VALUES (50, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:49', 0);
INSERT INTO `danmaku` VALUES (51, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:50', 2);
INSERT INTO `danmaku` VALUES (52, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:51', 2);
INSERT INTO `danmaku` VALUES (53, 1, 30, '刷点弹幕', '#000000', 0, 1, 1, '2020-02-26 07:54:51', 2);
INSERT INTO `danmaku` VALUES (54, 5, 30, '?', '#000000', 0, 1, 1, '2020-02-26 11:51:38', 0);
INSERT INTO `danmaku` VALUES (55, 2, 30, 'cd', '#000000', 0, 1, 1, '2020-02-27 10:25:20', 2);
INSERT INTO `danmaku` VALUES (56, 3, 30, 'ed', '#000000', 0, 1, 1, '2020-02-27 10:31:29', 0);
INSERT INTO `danmaku` VALUES (57, 1, 30, 'DASDAS', '#000000', 0, 1, 1, '2020-02-29 00:40:27', 0);
INSERT INTO `danmaku` VALUES (58, 1, 17, 'sss', '#ffffff', 0, 1, 0, '2020-02-29 01:12:52', 0);
INSERT INTO `danmaku` VALUES (59, 1, 17, 'sss', '#ffffff', 0, 1, 0, '2020-02-29 01:21:32', 0);
INSERT INTO `danmaku` VALUES (60, 1, 5, 'dasdsad', '#000000', 0, 1, 0, '2020-02-29 01:26:10', 0);
INSERT INTO `danmaku` VALUES (61, 1, 5, '呵呵', '#ffffff', 0, 0, 0, '2020-02-29 02:15:09', 0);
INSERT INTO `danmaku` VALUES (62, 1, 1577, '呵呵', '#ffffff', 0, 1, 0, '2020-02-29 02:15:36', 0);
INSERT INTO `danmaku` VALUES (63, 1, 221, '的撒旦', '#ffffff', 0, 1, 0, '2020-02-29 02:21:06', 0);
INSERT INTO `danmaku` VALUES (64, 1, 17, '1111111111', '#ffffff', 0, 1, 0, '2020-02-29 02:34:57', 0);
INSERT INTO `danmaku` VALUES (65, 1, 22, 'aa', '#ffffff', 0, 1, 0, '2020-02-29 02:44:13', 0);
INSERT INTO `danmaku` VALUES (66, 1, 20, 's', '#ffffff', 0, 1, 0, '2020-02-29 02:47:50', 0);
INSERT INTO `danmaku` VALUES (67, 1, 13, 'sss', '#ffffff', 0, 1, 0, '2020-02-29 02:49:51', 0);
INSERT INTO `danmaku` VALUES (68, 1, 18, 'ssss', '#ffffff', 0, 1, 0, '2020-02-29 02:53:14', 0);
INSERT INTO `danmaku` VALUES (69, 1, 17, 'sssssssss', '#ffffff', 0, 1, 0, '2020-02-29 03:00:47', 0);
INSERT INTO `danmaku` VALUES (70, 1, 13, 'ssssss', '#ffffff', 0, 1, 0, '2020-02-29 03:02:32', 0);
INSERT INTO `danmaku` VALUES (71, 1, 13, 'sadasf', '#ffffff', 0, 1, 0, '2020-02-29 03:06:10', 0);
INSERT INTO `danmaku` VALUES (72, 1, 15, 'sdfasdf', '#ffffff', 0, 1, 0, '2020-02-29 03:06:17', 0);
INSERT INTO `danmaku` VALUES (73, 1, 14, 'ssss', '#ffffff', 0, 1, 0, '2020-02-29 03:07:24', 0);
INSERT INTO `danmaku` VALUES (74, 1, 212, '???', '#ffffff', 0, 1, 0, '2020-02-29 03:50:57', 0);
INSERT INTO `danmaku` VALUES (75, 1, 79, '?', '#ffffff', 1, 1, 1, '2020-02-29 04:27:40', 0);
INSERT INTO `danmaku` VALUES (76, 1, 79, '???', '#ffffff', 2, 1, 1, '2020-02-29 04:27:46', 0);

-- ----------------------------
-- Table structure for follow
-- ----------------------------
DROP TABLE IF EXISTS `follow`;
CREATE TABLE `follow`  (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `follow_authorid` int(11) NOT NULL,
  `follow_userid` int(11) NOT NULL,
  PRIMARY KEY (`follow_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for shield
-- ----------------------------
DROP TABLE IF EXISTS `shield`;
CREATE TABLE `shield`  (
  `shield_id` int(11) NOT NULL AUTO_INCREMENT,
  `shield_userid` int(11) NOT NULL,
  `shield_banid` int(11) NOT NULL,
  PRIMARY KEY (`shield_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of shield
-- ----------------------------
INSERT INTO `shield` VALUES (1, 1, 15);
INSERT INTO `shield` VALUES (2, 1, 26);
INSERT INTO `shield` VALUES (3, 1, 25);
INSERT INTO `shield` VALUES (4, 1, 26);
INSERT INTO `shield` VALUES (6, 1, 15);
INSERT INTO `shield` VALUES (9, 1, 33);
INSERT INTO `shield` VALUES (15, 2, 29);
INSERT INTO `shield` VALUES (16, 2, 19);
INSERT INTO `shield` VALUES (17, 2, 16);
INSERT INTO `shield` VALUES (18, 2, 16);
INSERT INTO `shield` VALUES (19, 2, 17);
INSERT INTO `shield` VALUES (20, 2, 19);
INSERT INTO `shield` VALUES (21, 2, 25);
INSERT INTO `shield` VALUES (22, 1, 19);
INSERT INTO `shield` VALUES (23, 1, 44);
INSERT INTO `shield` VALUES (41, 1, 37);
INSERT INTO `shield` VALUES (42, 1, 48);
INSERT INTO `shield` VALUES (43, 1, 49);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` int(11) NULL DEFAULT NULL,
  `user_profilepic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_aboutself` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', '管理员', '502081700@qq.com', 0, 'head2.jpg', '我是管理员呵呵');
INSERT INTO `user` VALUES (2, 'user01', '123456', '前尘不共', NULL, 6, 'head1.jpg', '互联网行业社畜一枚');
INSERT INTO `user` VALUES (3, 'admin3', 'admin', 's', 's@s', 10, '0.jpg', NULL);

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `video_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `video__authorid` int(11) NOT NULL,
  `video_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `video_time` datetime(0) NOT NULL,
  `video_type` int(11) NOT NULL,
  `video_playcount` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`video_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, '仙剑四-回梦游仙笛子二胡合奏版(纯音乐)', 'video1.mp4', 'video1.jpg', 2, '自制 第一次用会声会影x4没看教程做的，花了一下午啊？？？？？', '2020-01-13 08:02:30', 2, 50000);
INSERT INTO `video` VALUES (2, '影  流  之  主  二  世', 'video2.mp4', 'video2.jpg', 2, '', '2020-01-14 10:05:06', 1, 0);
INSERT INTO `video` VALUES (3, '不  朽  尸  王', 'video3.mp4', 'video3.jpg', 1, '水水水水水水水水水水水水水水水水水水水', '2020-01-14 10:06:23', 1, 0);
INSERT INTO `video` VALUES (4, '影流之主', 'video4.mp4', 'video4.jpg', 1, NULL, '2020-01-14 10:07:26', 1, 0);
INSERT INTO `video` VALUES (5, '铁  幕  演  讲', 'video5.mp4', 'video5.jpg', 1, NULL, '2020-01-14 10:08:15', 1, 0);

-- ----------------------------
-- Table structure for videotype
-- ----------------------------
DROP TABLE IF EXISTS `videotype`;
CREATE TABLE `videotype`  (
  `videotype_id` int(11) NOT NULL AUTO_INCREMENT,
  `videotype_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`videotype_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of videotype
-- ----------------------------
INSERT INTO `videotype` VALUES (1, '动画');
INSERT INTO `videotype` VALUES (2, '音乐');
INSERT INTO `videotype` VALUES (3, '科技');
INSERT INTO `videotype` VALUES (4, '生活');
INSERT INTO `videotype` VALUES (5, '游戏');

SET FOREIGN_KEY_CHECKS = 1;
