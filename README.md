# 弹幕视频网站

#### 介绍
本系统主要目标是实现弹幕系统管理,提供用户注册和登录功能,


登录用户可以发送弹幕,点击右边弹幕列表进行单条弹幕屏蔽,<br>
举报违法弹幕,在个人中心添加关键词屏蔽.

视频作者可以选择隐藏视频中的弹幕.

管理员可以审核弹幕举报信息,管理所有视频的弹幕.

#### 软件架构
软件架构说明
基于SpringBoot+MyBatis+MySql+Jsp+LayUI的弹幕视频网站

弹幕播放器修改自 http://www.jq22.com/jquery-info2611<br>
DanmuPlayer (//github.com/chiruom/danmuplayer/) - Licensed under the MIT license


在线浏览
http://video.dawnbreaker.cn/

账号
管理员账号
admin  admin
视频作者账号
user01  123456
#### 安装教程

1.  导入数据库文件
2.  修改配置文件数据源

![播放器页面](https://images.gitee.com/uploads/images/2020/0229/184449_a39bbf65_2265904.jpeg "播放器页面")
![弹幕举报或屏蔽](https://images.gitee.com/uploads/images/2020/0229/184506_67bac8f9_2265904.jpeg "弹幕举报或屏蔽")
![举报页面](https://images.gitee.com/uploads/images/2020/0229/184552_b2de6acc_2265904.jpeg "举报页面")
![举报状态查看](https://images.gitee.com/uploads/images/2020/0229/184631_5633a57a_2265904.jpeg "举报状态查看")
![关键词屏蔽管理](https://images.gitee.com/uploads/images/2020/0229/184649_b684865d_2265904.jpeg "关键词屏蔽管理")
![视频弹幕管理](https://images.gitee.com/uploads/images/2020/0229/184722_4c1edfab_2265904.jpeg "视频弹幕管理")
![弹幕举报处理](https://images.gitee.com/uploads/images/2020/0229/184745_8a15de97_2265904.jpeg "弹幕举报处理")
![举报详细信息](https://images.gitee.com/uploads/images/2020/0229/184811_755eb047_2265904.jpeg "举报详细信息")
![恢复被举报弹幕](https://images.gitee.com/uploads/images/2020/0229/184838_67b6db7e_2265904.jpeg "恢复被举报弹幕")
![管理所有视频](https://images.gitee.com/uploads/images/2020/0302/084332_7f27ad54_2265904.jpeg "管理所有视频")

![主页](https://images.gitee.com/uploads/images/2020/0229/184417_18c2b3cd_2265904.jpeg "主页")