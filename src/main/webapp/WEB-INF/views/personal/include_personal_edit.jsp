<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/1/20
  Time: 下午 9:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/layui/layui.js"></script>
    <link rel="stylesheet" type="text/css" href="static/css/personal.css" />
</head>
<body>
<div class="security-right">
    <div class="security-right-title">
        <span class="security-right-title-icon"></span>
        <span class="security-right-title-text">修改资料</span>

    </div>

    <div class="user-setting-warp">
        <div>
            <form action="/do/personal_edit" method="post" >
                <c:if test="${not empty InfoMsg}"><h1 style="margin-left: 50px;margin-bottom: 20px;color: #00B5E5;">${InfoMsg}</h1></c:if>
                <div class="user-nick-name">
                    <label class="el-form-item__label">昵称</label>
                    <div class="el-input"><input autocomplete="off" placeholder="昵称" type="text" name="userNickName" maxlength="16" validateevent="true" class="el-input__inner" value="${LoginUser.userNickname}"></div>
                </div>

                <div class="el-form-item user-my-sign">
                    <label class="el-form-item__label">我的签名</label>
                    <div class="el-form-item__content">
                        <div class="el-textarea">
                            <textarea name="userAboutself" rows="6" autocomplete="off"  type="textarea" validateevent="true"  placeholder="请输入您的签名" cols="el-textarea__inner">${LoginUser.userAboutself}</textarea>
                        </div>
                    </div>
                </div>
                <div class="user-nick-name touteng">
                    <label class="el-form-item__label">密码</label>
                    <div class="el-input"><input autocomplete="off" placeholder="密码" name="userPassword" type="password" maxlength="16" validateevent="true" class="el-input__inner" value="${LoginUser.userPassword}"></div>
                </div>
                <hr class="layui-bg-blue">
                <button type="submit" class="layui-btn layui-btn-radius layui-btn-normal" style="margin-left: 100px;">提交修改</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
