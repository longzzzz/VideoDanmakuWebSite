<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/1/20
  Time: 下午 7:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/layui/layui.js"></script>
    <link rel="stylesheet" type="text/css" href="static/css/personal.css" />
</head>
<body>
<div class="security-right">
    <div class="security-right-title">
        <span class="security-right-title-icon"></span>
        <span class="security-right-title-text">个人中心</span>

    </div>
    <div class="secuity-right-home">
        <div class="index-info">
            <div class="home-head">
                <img src="static/headResouces/${LoginUser.userProfilepic}" />
            </div>
            <div class="home-right">
                <div class="home-top-msg">
                    <span class="home-top-msg-name home-top-msg-name-color">${LoginUser.userNickname}</span>
                    <span class="home-userstatus">${LoginUser.userTypeName}</span>
                </div>
                <div class="home-top-bp">
                    <a class="coin-link layui-icon layui-icon-video"></a><span class="curren-b-num">投稿数:10</span>
                    <a class="coin-link layui-icon layui-icon-play"></a><span class="curren-b-num">播放数:10</span>
                </div>
            </div>
        </div>

    </div>

</div>
</body>
</html>
