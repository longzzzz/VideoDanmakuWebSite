<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/2/26
  Time: 下午 5:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="static/css/personal.css" />
    <link rel="stylesheet" type="text/css" href="static/css/MyVideo.css" />
    <link rel="stylesheet" type="text/css" href="static/css/DanmakuManager.css" />
    <link rel="stylesheet" type="text/css" href="static/css/AdminComplaintManager.css" />
</head>
<body>
<jsp:include page="/head"></jsp:include>
<div class="security_content">
    <div class="security-left">
        <span class="security-title"><i class="layui-icon layui-icon-console"></i>&nbsp;&nbsp;&nbsp;控制台</span>
        <ul class="security-ul">
            <li class="security-list <c:if test="${type == 'main'}">router-link-exact-active on</c:if>">
                <a href="/adminController/main"><i class="layui-icon layui-icon-home"></i><span class="security-nav-name">首页</span></a>
            </li>
            <li class="security-list <c:if test="${type == 'ComplaintManager'}">router-link-exact-active on</c:if>">
                <a href="/adminController/ComplaintManager"><i class="layui-icon layui-icon-template-1"></i><span class="security-nav-name">弹幕举报处理</span></a>
            </li>
            <li class="security-list <c:if test="${type == 'VideoDanmakuManager' or type == 'AdminDanmakuManager'}">router-link-exact-active on</c:if>">
                <a href="/adminController/VideoDanmakuManager"><i class="layui-icon layui-icon-video"></i><span class="security-nav-name">视频弹幕管理</span></a>
            </li>

        </ul>
    </div>

    <div class="security-right">
        <c:if test="${type == 'main'}"> <div class="security-right-title"><span class="security-right-title-icon"></span><span class="security-right-title-text">首页</span></div></c:if>
        <c:if test="${type == 'ComplaintManager'}"><jsp:include page="/adminControllerRight/ComplaintManagerPage?type=unpass"></jsp:include></c:if>
        <c:if test="${type == 'PassedComplaintManager'}"><jsp:include page="/adminControllerRight/ComplaintManagerPage?type=passed"></jsp:include></c:if>
        <c:if test="${type == 'VideoDanmakuManager'}"><jsp:include page="/adminControllerRight/VideoDanmakuManager"></jsp:include></c:if>
        <c:if test="${type == 'AdminDanmakuManager'}"><jsp:include page="/adminControllerRight/AdminDanmakuManager/${videoId}"></jsp:include></c:if>


    </div>
</div>
</body>
</html>
