<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/2/26
  Time: 下午 5:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/js/jquery-3.4.1.min.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/js/Complaint.js"></script>

    <link rel="stylesheet" type="text/css" href="static/css/personal.css" />
    <link rel="stylesheet" type="text/css" href="static/css/MyVideo.css" />
    <link rel="stylesheet" type="text/css" href="static/css/DanmakuManager.css" />
    <link rel="stylesheet" type="text/css" href="static/css/ComplaintManager.css" />
    <link rel="stylesheet" type="text/css" href="static/css/AdminComplaintManager.css" />
</head>
<body>
<div class="security-right-title">
    <span class="security-right-title-icon"></span>
    <span class="security-right-title-text">弹幕举报处理</span>
</div>
<div class="security-right-title">
    <span class="security-right-title-text "><a href="/adminController/ComplaintManager">未处理</a></span>
    <span class="security-right-title-text"><a href="/adminController/PassedComplaintManager">已处理</a></span>
</div>

<div id="pageList" style="margin-left: 20px"></div>
<ul class="danmakuList layui-elip">

    <li><span class="VideoTitle">视频名称</span><span class="danmakutime">时间</span><span class="danmakucontent">弹幕内容</span><span class="danmakutype">举报人数</span><span class="danmakustatus">状态</span><span class="danmakumanager">操作</span></li><hr>
    <ul id="biuuu_city_list"></ul>
<%--    <li>--%>
<%--        <span class="VideoTitle layui-elip" title="视频名称视频名称视频名称视频名称">视频名称视频名称视频名称视频名称</span>--%>
<%--        <span class="danmakutime">时间</span>--%>
<%--        <span class="danmakucontent layui-elip" title="内sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss容">内sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss容</span>--%>
<%--        <span class="danmakutype">5人</span>--%>
<%--        <span class="danmakustatus">正常</span>--%>
<%--        <span class="danmakumanager">--%>
<%--							<div class="layui-btn-group">--%>
<%--								<button type="button" class="layui-btn layui-btn-sm">查看详细</button>--%>
<%--							</div>--%>
<%--							<div class="layui-btn-group">--%>
<%--  								<button type="button" class="layui-btn layui-btn-sm layui-btn-danger">通过举报</button>--%>
<%--  								<button type="button" class="layui-btn layui-btn-sm layui-btn-danger">驳回举报</button>--%>
<%--							</div>--%>
<%--						</span>--%>
<%--        <div class="hidden"><i class="layui-icon layui-icon-ok-circle ok"></i></div>--%>
<%--    </li><hr>--%>

</ul>
<script>
    layui.use(['laypage', 'layer','element'], function(){
        var laypage = layui.laypage
            ,layer = layui.layer;
        var element = layui.element;
        //将一段数组分页展示

        //测试数据
        var datatsouce = "<C:if test="${type =='unpass' }">unpass</C:if><C:if test="${type == 'passed'}">passed</C:if>";

        var data = <C:if test="${type =='unpass' }">getunPassComplaint()</C:if><C:if test="${type == 'passed'}">getPassComplaint()</C:if>;

        //调用分页
        laypage.render({
            elem: 'pageList'
            ,limit: 15
            ,count: data.length
            ,jump: function(obj){
                //模拟渲染
                document.getElementById('biuuu_city_list').innerHTML = function(){
                    var arr = []
                        ,thisData = data.concat().splice(obj.curr*obj.limit - obj.limit, obj.limit);
                    layui.each(thisData, function(index, item){
                        var div = document.createElement('div');
                        var Li = document.createElement('li');
                        var Span1 = document.createElement('span');var Span2 = document.createElement('span');
                        var Span3 = document.createElement('span');var Span4 = document.createElement('span');
                        var Span5 = document.createElement('span');var Span6 = document.createElement('span');
                        var Div1 = document.createElement('div');
                        var Btn1 = document.createElement('button');
                        var Div2 = document.createElement('div');
                        var Btn2 = document.createElement('button');
                        var Btn3 = document.createElement('button');
                        var Btn4 = document.createElement('button');
                        var Div3 = document.createElement('div');
                        var i1 = document.createElement('i');
                        var a1 = document.createElement('a');


                        Li.id = "danmaku"+item.danmakuId;

                        Span1.className = "VideoTitle layui-elip";
                        Span1.title = item.video.videoName;

                        a1.href = "Video/"+item.video.videoId;
                        a1.textContent = item.video.videoName;

                        Span2.className = "danmakutime";
                        Span2.textContent = item.danmaku.danmakuStringTime;


                        Span3.className = "danmakucontent layui-elip";
                        Span3.title = item.danmaku.danmakuContent;
                        Span3.textContent = item.danmaku.danmakuContent;

                        Span4.className = "danmakutype";
                        Span4.textContent = item.complaintCount+"人";

                        Span5.className = "danmakutime";
                        Span5.textContent = item.danmaku.stringstatus;

                        Span6.className = "danmakumanager";

                        Div1.className = "layui-btn-group";
                        Btn1.type = "button";
                        Btn1.className = "layui-btn layui-btn-sm";
                        Btn1.textContent = "查看详细";
                        var GroupMsg;
                        if (datatsouce == 'passed')
                            GroupMsg = "checkThePassComplaintGroup("+item.danmakuId+")";
                        else
                            GroupMsg = "checkTheComplaintGroup("+item.danmakuId+")";
                        $(Btn1).attr("onclick",GroupMsg);
                        // Btn1.addEventListener('click', function() {
                        //     console.log('click');
                        // }, true);
                        // Btn1.onclick = function (){checkTheComplaintGroup(item.danmakuId)};
                        // Btn1.onclick = checkTheComplaintGroup(item.danmakuId);

                        Div2.className = "layui-btn-group";
                        // Btn2.type = "button";
                        Btn2.className = "layui-btn layui-btn-sm layui-btn-danger";
                        Btn2.textContent = "通过举报";
                        $(Btn2).attr("onclick","switchTheComplaintStaus("+item.danmakuId+","+2+")");
                        // Btn3.type = "button";
                        Btn3.className = "layui-btn layui-btn-sm layui-btn-danger";
                        Btn3.textContent = "驳回举报";
                        $(Btn3).attr("onclick","switchTheComplaintStaus("+item.danmakuId+","+1+")");

                        Btn4.className = "layui-btn layui-btn-sm layui-btn-normal";
                        Btn4.textContent = "恢复弹幕";
                        $(Btn4).attr("onclick","switchTheComplaintStaus("+item.danmakuId+","+1+")");

                        Div3.className = "hidden";
                        Div3.id = "hidden"+item.danmakuId;
                        i1.className = "layui-icon layui-icon-ok-circle ok";

                        Span1.appendChild(a1);
                        Div1.appendChild(Btn1);

                        if (datatsouce == 'passed')
                            Div2.appendChild(Btn4);
                        else
                            {
                                Div2.appendChild(Btn2);
                                Div2.appendChild(Btn3);
                            }

                        Div3.appendChild(i1);
                        Span6.appendChild(Div1);
                        Span6.appendChild(Div2);
                        Li.appendChild(Span1);
                        Li.appendChild(Span2);
                        Li.appendChild(Span3);
                        Li.appendChild(Span4);
                        Li.appendChild(Span5);
                        Li.appendChild(Span6);
                        Li.appendChild(Div3);
                        div.appendChild(Li);
                        arr.push(div.innerHTML+"<hr>");
                    });
                    return arr.join('');
                }();
            }
        });

    });


</script>
</body>
</html>
