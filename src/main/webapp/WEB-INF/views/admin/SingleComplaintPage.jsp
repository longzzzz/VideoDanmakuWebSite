<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/2/27
  Time: 上午 9:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="static/css/personal.css" />
    <link rel="stylesheet" type="text/css" href="static/css/MyVideo.css" />
    <link rel="stylesheet" type="text/css" href="static/css/DanmakuManager.css" />
    <link rel="stylesheet" type="text/css" href="static/css/ComplaintManager.css" />
    <link rel="stylesheet" type="text/css" href="static/css/AdminComplaintManager.css" />
</head>
<body>
<div class="complaintList">
    <li class="list-item clearfix fakeDanmu-item new">
        <a class="cover">
            <img src="static/videoResouces/${complaintList.video.videoPic}" />
        </a>
        <div class="c">
            <div class="title-row">
                <a title="${complaintList.video.videoName}" class="title">
                    ${complaintList.video.videoName}
                </a>
            </div>
            <div title="${complaintList.video.videoInfo}" class="desc">
                ${complaintList.video.videoInfo}
            </div>
        </div>
    </li>
    <ul class="danmakuList layui-elip">
        <li><span class="danmakuId">弹幕ID</span><span class="danmakutime">时间</span><span class="danmakucontent">内容</span><span class="danmakusendtime">发送日期</span></li><hr>
        <li>
            <span class="danmakuId">${complaintList.danmaku.danmakuId}</span>
            <span class="danmakutime">${complaintList.danmaku.danmakuStringTime}</span>
            <span class="danmakucontent layui-elip" title="${complaintList.danmaku.danmakuContent}">${complaintList.danmaku.danmakuContent}</span>
            <span class="danmakusendtime" title="${complaintList.danmaku.stringLongSendtime}">${complaintList.danmaku.stringSendtime}</span>
        </li><hr>
    </ul>
    <ul class="danmakuList layui-elip " style="overflow: auto;">
        <li><span class="danmakuId">举报人ID</span><span class="complainttype">举报类型</span><span class="complaint_describe">举报理由</span></li><hr>
        <C:forEach items="${complaintList.complaints}" var="complaint" varStatus="status">
        <li>
            <span class="danmakuId">${complaint.complaintUserId}</span>
            <span class="complainttype" title="${complaint.complainttype.complainttypeName}">${complaint.complainttype.complainttypeName}</span>
            <span class="complaint_describe" title="${complaint.complaintDescribe}">${complaint.complaintDescribe}</span>
        </li><hr/>
        </C:forEach>
    </ul>
</div>
</body>
</html>
