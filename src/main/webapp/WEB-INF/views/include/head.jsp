<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 50208
  Date: 2020/1/13
  Time: 下午 5:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <base href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/layui/layui.js"></script>
</head>
<body>
<inc>
<ul class="layui-nav">
    <li class="layui-nav-item">
        <a href="">首页</a>
    </li>
    <c:if test="${empty LoginUser}">
        <li class="layui-nav-item" style="float: right">
            <a href="/Login">登录账号</a>
        </li>
        <li class="layui-nav-item" style="float: right">
            <a href="/Register">注册</a>
        </li>
    </c:if>
    <c:if test="${not empty LoginUser}">
        <li class="layui-nav-item"  style="float: right">
            <a href="">通知<span class="layui-badge">5</span></a>
        </li>
    <li class="layui-nav-item"  style="float: right">
        <a><img src="static/headResouces/${LoginUser.userProfilepic}" class="layui-nav-img">我</a>
        <dl class="layui-nav-child">
            <dd><a href="personal/personal_self">个人中心</a></dd>
        <c:if test="${LoginUser.userType==0}"><dd><a href="/adminController/main">管理员管理</a></dd></c:if>
            <dd><a href="/Logout">退出账号</a></dd>
        </dl>
    </li>

    </c:if>
</ul>
</inc>
<script>
    layui.use(['element',function () {
        var element = layui.element;
    }])
</script>

</body>
</html>
