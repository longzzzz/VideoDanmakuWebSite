package cn.hongpeiming.videodanmakusite.pojo;

import java.util.ArrayList;
import java.util.List;

public class BanKeywordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BanKeywordExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBanKeywordIdIsNull() {
            addCriterion("ban_keyword_id is null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdIsNotNull() {
            addCriterion("ban_keyword_id is not null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdEqualTo(Integer value) {
            addCriterion("ban_keyword_id =", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdNotEqualTo(Integer value) {
            addCriterion("ban_keyword_id <>", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdGreaterThan(Integer value) {
            addCriterion("ban_keyword_id >", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ban_keyword_id >=", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdLessThan(Integer value) {
            addCriterion("ban_keyword_id <", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdLessThanOrEqualTo(Integer value) {
            addCriterion("ban_keyword_id <=", value, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdIn(List<Integer> values) {
            addCriterion("ban_keyword_id in", values, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdNotIn(List<Integer> values) {
            addCriterion("ban_keyword_id not in", values, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdBetween(Integer value1, Integer value2) {
            addCriterion("ban_keyword_id between", value1, value2, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ban_keyword_id not between", value1, value2, "banKeywordId");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridIsNull() {
            addCriterion("ban_keyword_userid is null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridIsNotNull() {
            addCriterion("ban_keyword_userid is not null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridEqualTo(Integer value) {
            addCriterion("ban_keyword_userid =", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridNotEqualTo(Integer value) {
            addCriterion("ban_keyword_userid <>", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridGreaterThan(Integer value) {
            addCriterion("ban_keyword_userid >", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("ban_keyword_userid >=", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridLessThan(Integer value) {
            addCriterion("ban_keyword_userid <", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridLessThanOrEqualTo(Integer value) {
            addCriterion("ban_keyword_userid <=", value, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridIn(List<Integer> values) {
            addCriterion("ban_keyword_userid in", values, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridNotIn(List<Integer> values) {
            addCriterion("ban_keyword_userid not in", values, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridBetween(Integer value1, Integer value2) {
            addCriterion("ban_keyword_userid between", value1, value2, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("ban_keyword_userid not between", value1, value2, "banKeywordUserid");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringIsNull() {
            addCriterion("ban_keyword_string is null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringIsNotNull() {
            addCriterion("ban_keyword_string is not null");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringEqualTo(String value) {
            addCriterion("ban_keyword_string =", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringNotEqualTo(String value) {
            addCriterion("ban_keyword_string <>", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringGreaterThan(String value) {
            addCriterion("ban_keyword_string >", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringGreaterThanOrEqualTo(String value) {
            addCriterion("ban_keyword_string >=", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringLessThan(String value) {
            addCriterion("ban_keyword_string <", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringLessThanOrEqualTo(String value) {
            addCriterion("ban_keyword_string <=", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringLike(String value) {
            addCriterion("ban_keyword_string like", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringNotLike(String value) {
            addCriterion("ban_keyword_string not like", value, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringIn(List<String> values) {
            addCriterion("ban_keyword_string in", values, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringNotIn(List<String> values) {
            addCriterion("ban_keyword_string not in", values, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringBetween(String value1, String value2) {
            addCriterion("ban_keyword_string between", value1, value2, "banKeywordString");
            return (Criteria) this;
        }

        public Criteria andBanKeywordStringNotBetween(String value1, String value2) {
            addCriterion("ban_keyword_string not between", value1, value2, "banKeywordString");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}