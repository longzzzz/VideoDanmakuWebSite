package cn.hongpeiming.videodanmakusite.pojo;

public class Complaint {
    private Integer complaintId;

    private Integer complaintDanmakuId;

    private Integer complaintUserId;

    private Integer complaintType;

    private String complaintDescribe;

    private Integer complaintStatus;

    private String stringStatus;

    private Danmaku danmaku;

    private Video video;

    private Complainttype complainttype;


    public Integer getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(Integer complaintId) {
        this.complaintId = complaintId;
    }

    public Integer getComplaintDanmakuId() {
        return complaintDanmakuId;
    }

    public void setComplaintDanmakuId(Integer complaintDanmakuId) {
        this.complaintDanmakuId = complaintDanmakuId;
    }

    public Integer getComplaintUserId() {
        return complaintUserId;
    }

    public void setComplaintUserId(Integer complaintUserId) {
        this.complaintUserId = complaintUserId;
    }

    public Integer getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(Integer complaintType) {
        this.complaintType = complaintType;
    }

    public String getComplaintDescribe() {
        return complaintDescribe;
    }

    public void setComplaintDescribe(String complaintDescribe) {
        this.complaintDescribe = complaintDescribe == null ? null : complaintDescribe.trim();
    }

    public Integer getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(Integer complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public Danmaku getDanmaku() {
        return danmaku;
    }

    public void setDanmaku(Danmaku danmaku) {
        this.danmaku = danmaku;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Complainttype getComplainttype() {
        return complainttype;
    }

    public void setComplainttype(Complainttype complainttype) {
        this.complainttype = complainttype;
    }

    public String getStringStatus() {
        if (complaintStatus==0)
            return "未处理";
        else if (complaintStatus==1)
            return "被驳回";
        else
            return "已处理";
    }

    public void setStringStatus(String stringStatus) {
        this.stringStatus = stringStatus;
    }
}