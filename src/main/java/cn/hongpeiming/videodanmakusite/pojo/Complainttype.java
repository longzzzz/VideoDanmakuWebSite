package cn.hongpeiming.videodanmakusite.pojo;

public class Complainttype {
    private Integer complainttypeId;

    private String complainttypeName;

    public Integer getComplainttypeId() {
        return complainttypeId;
    }

    public void setComplainttypeId(Integer complainttypeId) {
        this.complainttypeId = complainttypeId;
    }

    public String getComplainttypeName() {
        return complainttypeName;
    }

    public void setComplainttypeName(String complainttypeName) {
        this.complainttypeName = complainttypeName == null ? null : complainttypeName.trim();
    }
}