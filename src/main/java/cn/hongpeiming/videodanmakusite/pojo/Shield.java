package cn.hongpeiming.videodanmakusite.pojo;

public class Shield {
    private Integer shieldId;

    private Integer shieldUserid;

    private Integer shieldBanid;

    public Integer getShieldId() {
        return shieldId;
    }

    public void setShieldId(Integer shieldId) {
        this.shieldId = shieldId;
    }

    public Integer getShieldUserid() {
        return shieldUserid;
    }

    public void setShieldUserid(Integer shieldUserid) {
        this.shieldUserid = shieldUserid;
    }

    public Integer getShieldBanid() {
        return shieldBanid;
    }

    public void setShieldBanid(Integer shieldBanid) {
        this.shieldBanid = shieldBanid;
    }
}