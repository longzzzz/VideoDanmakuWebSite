package cn.hongpeiming.videodanmakusite.pojo;

import java.util.ArrayList;
import java.util.List;

public class ComplaintExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ComplaintExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andComplaintIdIsNull() {
            addCriterion("complaint_id is null");
            return (Criteria) this;
        }

        public Criteria andComplaintIdIsNotNull() {
            addCriterion("complaint_id is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintIdEqualTo(Integer value) {
            addCriterion("complaint_id =", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdNotEqualTo(Integer value) {
            addCriterion("complaint_id <>", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdGreaterThan(Integer value) {
            addCriterion("complaint_id >", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("complaint_id >=", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdLessThan(Integer value) {
            addCriterion("complaint_id <", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdLessThanOrEqualTo(Integer value) {
            addCriterion("complaint_id <=", value, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdIn(List<Integer> values) {
            addCriterion("complaint_id in", values, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdNotIn(List<Integer> values) {
            addCriterion("complaint_id not in", values, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdBetween(Integer value1, Integer value2) {
            addCriterion("complaint_id between", value1, value2, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintIdNotBetween(Integer value1, Integer value2) {
            addCriterion("complaint_id not between", value1, value2, "complaintId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdIsNull() {
            addCriterion("complaint_danmaku_id is null");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdIsNotNull() {
            addCriterion("complaint_danmaku_id is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdEqualTo(Integer value) {
            addCriterion("complaint_danmaku_id =", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdNotEqualTo(Integer value) {
            addCriterion("complaint_danmaku_id <>", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdGreaterThan(Integer value) {
            addCriterion("complaint_danmaku_id >", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("complaint_danmaku_id >=", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdLessThan(Integer value) {
            addCriterion("complaint_danmaku_id <", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdLessThanOrEqualTo(Integer value) {
            addCriterion("complaint_danmaku_id <=", value, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdIn(List<Integer> values) {
            addCriterion("complaint_danmaku_id in", values, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdNotIn(List<Integer> values) {
            addCriterion("complaint_danmaku_id not in", values, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdBetween(Integer value1, Integer value2) {
            addCriterion("complaint_danmaku_id between", value1, value2, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintDanmakuIdNotBetween(Integer value1, Integer value2) {
            addCriterion("complaint_danmaku_id not between", value1, value2, "complaintDanmakuId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdIsNull() {
            addCriterion("complaint_user_id is null");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdIsNotNull() {
            addCriterion("complaint_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdEqualTo(Integer value) {
            addCriterion("complaint_user_id =", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdNotEqualTo(Integer value) {
            addCriterion("complaint_user_id <>", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdGreaterThan(Integer value) {
            addCriterion("complaint_user_id >", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("complaint_user_id >=", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdLessThan(Integer value) {
            addCriterion("complaint_user_id <", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("complaint_user_id <=", value, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdIn(List<Integer> values) {
            addCriterion("complaint_user_id in", values, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdNotIn(List<Integer> values) {
            addCriterion("complaint_user_id not in", values, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdBetween(Integer value1, Integer value2) {
            addCriterion("complaint_user_id between", value1, value2, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("complaint_user_id not between", value1, value2, "complaintUserId");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeIsNull() {
            addCriterion("complaint_type is null");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeIsNotNull() {
            addCriterion("complaint_type is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeEqualTo(Integer value) {
            addCriterion("complaint_type =", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeNotEqualTo(Integer value) {
            addCriterion("complaint_type <>", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeGreaterThan(Integer value) {
            addCriterion("complaint_type >", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("complaint_type >=", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeLessThan(Integer value) {
            addCriterion("complaint_type <", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeLessThanOrEqualTo(Integer value) {
            addCriterion("complaint_type <=", value, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeIn(List<Integer> values) {
            addCriterion("complaint_type in", values, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeNotIn(List<Integer> values) {
            addCriterion("complaint_type not in", values, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeBetween(Integer value1, Integer value2) {
            addCriterion("complaint_type between", value1, value2, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("complaint_type not between", value1, value2, "complaintType");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeIsNull() {
            addCriterion("complaint_describe is null");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeIsNotNull() {
            addCriterion("complaint_describe is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeEqualTo(String value) {
            addCriterion("complaint_describe =", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeNotEqualTo(String value) {
            addCriterion("complaint_describe <>", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeGreaterThan(String value) {
            addCriterion("complaint_describe >", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeGreaterThanOrEqualTo(String value) {
            addCriterion("complaint_describe >=", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeLessThan(String value) {
            addCriterion("complaint_describe <", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeLessThanOrEqualTo(String value) {
            addCriterion("complaint_describe <=", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeLike(String value) {
            addCriterion("complaint_describe like", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeNotLike(String value) {
            addCriterion("complaint_describe not like", value, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeIn(List<String> values) {
            addCriterion("complaint_describe in", values, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeNotIn(List<String> values) {
            addCriterion("complaint_describe not in", values, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeBetween(String value1, String value2) {
            addCriterion("complaint_describe between", value1, value2, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintDescribeNotBetween(String value1, String value2) {
            addCriterion("complaint_describe not between", value1, value2, "complaintDescribe");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusIsNull() {
            addCriterion("complaint_status is null");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusIsNotNull() {
            addCriterion("complaint_status is not null");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusEqualTo(Integer value) {
            addCriterion("complaint_status =", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusNotEqualTo(Integer value) {
            addCriterion("complaint_status <>", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusGreaterThan(Integer value) {
            addCriterion("complaint_status >", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("complaint_status >=", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusLessThan(Integer value) {
            addCriterion("complaint_status <", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusLessThanOrEqualTo(Integer value) {
            addCriterion("complaint_status <=", value, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusIn(List<Integer> values) {
            addCriterion("complaint_status in", values, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusNotIn(List<Integer> values) {
            addCriterion("complaint_status not in", values, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusBetween(Integer value1, Integer value2) {
            addCriterion("complaint_status between", value1, value2, "complaintStatus");
            return (Criteria) this;
        }

        public Criteria andComplaintStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("complaint_status not between", value1, value2, "complaintStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}