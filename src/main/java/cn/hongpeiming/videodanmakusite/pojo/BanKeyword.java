package cn.hongpeiming.videodanmakusite.pojo;

public class BanKeyword {
    private Integer banKeywordId;

    private Integer banKeywordUserid;

    private String banKeywordString;

    public Integer getBanKeywordId() {
        return banKeywordId;
    }

    public void setBanKeywordId(Integer banKeywordId) {
        this.banKeywordId = banKeywordId;
    }

    public Integer getBanKeywordUserid() {
        return banKeywordUserid;
    }

    public void setBanKeywordUserid(Integer banKeywordUserid) {
        this.banKeywordUserid = banKeywordUserid;
    }

    public String getBanKeywordString() {
        return banKeywordString;
    }

    public void setBanKeywordString(String banKeywordString) {
        this.banKeywordString = banKeywordString == null ? null : banKeywordString.trim();
    }
}