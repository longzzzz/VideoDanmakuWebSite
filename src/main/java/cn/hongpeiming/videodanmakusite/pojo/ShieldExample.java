package cn.hongpeiming.videodanmakusite.pojo;

import java.util.ArrayList;
import java.util.List;

public class ShieldExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ShieldExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andShieldIdIsNull() {
            addCriterion("shield_id is null");
            return (Criteria) this;
        }

        public Criteria andShieldIdIsNotNull() {
            addCriterion("shield_id is not null");
            return (Criteria) this;
        }

        public Criteria andShieldIdEqualTo(Integer value) {
            addCriterion("shield_id =", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdNotEqualTo(Integer value) {
            addCriterion("shield_id <>", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdGreaterThan(Integer value) {
            addCriterion("shield_id >", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("shield_id >=", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdLessThan(Integer value) {
            addCriterion("shield_id <", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdLessThanOrEqualTo(Integer value) {
            addCriterion("shield_id <=", value, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdIn(List<Integer> values) {
            addCriterion("shield_id in", values, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdNotIn(List<Integer> values) {
            addCriterion("shield_id not in", values, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdBetween(Integer value1, Integer value2) {
            addCriterion("shield_id between", value1, value2, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldIdNotBetween(Integer value1, Integer value2) {
            addCriterion("shield_id not between", value1, value2, "shieldId");
            return (Criteria) this;
        }

        public Criteria andShieldUseridIsNull() {
            addCriterion("shield_userid is null");
            return (Criteria) this;
        }

        public Criteria andShieldUseridIsNotNull() {
            addCriterion("shield_userid is not null");
            return (Criteria) this;
        }

        public Criteria andShieldUseridEqualTo(Integer value) {
            addCriterion("shield_userid =", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridNotEqualTo(Integer value) {
            addCriterion("shield_userid <>", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridGreaterThan(Integer value) {
            addCriterion("shield_userid >", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("shield_userid >=", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridLessThan(Integer value) {
            addCriterion("shield_userid <", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridLessThanOrEqualTo(Integer value) {
            addCriterion("shield_userid <=", value, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridIn(List<Integer> values) {
            addCriterion("shield_userid in", values, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridNotIn(List<Integer> values) {
            addCriterion("shield_userid not in", values, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridBetween(Integer value1, Integer value2) {
            addCriterion("shield_userid between", value1, value2, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("shield_userid not between", value1, value2, "shieldUserid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidIsNull() {
            addCriterion("shield_banid is null");
            return (Criteria) this;
        }

        public Criteria andShieldBanidIsNotNull() {
            addCriterion("shield_banid is not null");
            return (Criteria) this;
        }

        public Criteria andShieldBanidEqualTo(Integer value) {
            addCriterion("shield_banid =", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidNotEqualTo(Integer value) {
            addCriterion("shield_banid <>", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidGreaterThan(Integer value) {
            addCriterion("shield_banid >", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidGreaterThanOrEqualTo(Integer value) {
            addCriterion("shield_banid >=", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidLessThan(Integer value) {
            addCriterion("shield_banid <", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidLessThanOrEqualTo(Integer value) {
            addCriterion("shield_banid <=", value, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidIn(List<Integer> values) {
            addCriterion("shield_banid in", values, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidNotIn(List<Integer> values) {
            addCriterion("shield_banid not in", values, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidBetween(Integer value1, Integer value2) {
            addCriterion("shield_banid between", value1, value2, "shieldBanid");
            return (Criteria) this;
        }

        public Criteria andShieldBanidNotBetween(Integer value1, Integer value2) {
            addCriterion("shield_banid not between", value1, value2, "shieldBanid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}