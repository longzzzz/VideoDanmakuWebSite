package cn.hongpeiming.videodanmakusite.pojo;

import java.util.List;

public class ComplaintList {
    private Integer danmakuId;
    private Integer complaintCount;
    private Video video;
    private Danmaku danmaku;
    private List<Complaint> complaints;

    public Integer getDanmakuId() {
        return danmakuId;
    }

    public void setDanmakuId(Integer danmakuId) {
        this.danmakuId = danmakuId;
    }

    public Integer getComplaintCount() {
        return complaintCount;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public void setComplaintCount(Integer complaintCount) {
        this.complaintCount = complaintCount;
    }

    public Danmaku getDanmaku() {
        return danmaku;
    }

    public void setDanmaku(Danmaku danmaku) {
        this.danmaku = danmaku;
    }

    public List<Complaint> getComplaints() {
        return complaints;
    }

    public void setComplaints(List<Complaint> complaints) {
        this.complaints = complaints;
    }
}
