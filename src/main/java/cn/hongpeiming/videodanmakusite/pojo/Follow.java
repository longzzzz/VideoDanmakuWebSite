package cn.hongpeiming.videodanmakusite.pojo;

public class Follow {
    private Integer followId;

    private Integer followAuthorid;

    private Integer followUserid;

    public Integer getFollowId() {
        return followId;
    }

    public void setFollowId(Integer followId) {
        this.followId = followId;
    }

    public Integer getFollowAuthorid() {
        return followAuthorid;
    }

    public void setFollowAuthorid(Integer followAuthorid) {
        this.followAuthorid = followAuthorid;
    }

    public Integer getFollowUserid() {
        return followUserid;
    }

    public void setFollowUserid(Integer followUserid) {
        this.followUserid = followUserid;
    }
}