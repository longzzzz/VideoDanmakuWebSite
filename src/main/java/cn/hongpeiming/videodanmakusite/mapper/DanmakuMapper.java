package cn.hongpeiming.videodanmakusite.mapper;

import cn.hongpeiming.videodanmakusite.pojo.Danmaku;
import cn.hongpeiming.videodanmakusite.pojo.DanmakuExample;
import java.util.List;

import cn.hongpeiming.videodanmakusite.utils.PageResult;
import org.apache.ibatis.annotations.Param;

public interface DanmakuMapper {
    long countByExample(DanmakuExample example);

    int deleteByExample(DanmakuExample example);

    int deleteByPrimaryKey(Integer danmakuId);

    int insert(Danmaku record);

    int insertSelective(Danmaku record);

    List<Danmaku> selectByExample(DanmakuExample example);

    Danmaku selectByPrimaryKey(Integer danmakuId);

    int updateByExampleSelective(@Param("record") Danmaku record, @Param("example") DanmakuExample example);

    int updateByExample(@Param("record") Danmaku record, @Param("example") DanmakuExample example);

    int updateByPrimaryKeySelective(Danmaku record);

    int updateByPrimaryKey(Danmaku record);

    List<Danmaku> selectPage(Integer danmaku_videoid);
}