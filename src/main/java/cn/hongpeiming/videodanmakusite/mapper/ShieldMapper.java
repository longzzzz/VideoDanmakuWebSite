package cn.hongpeiming.videodanmakusite.mapper;

import cn.hongpeiming.videodanmakusite.pojo.Shield;
import cn.hongpeiming.videodanmakusite.pojo.ShieldExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ShieldMapper {
    long countByExample(ShieldExample example);

    int deleteByExample(ShieldExample example);

    int deleteByPrimaryKey(Integer shieldId);

    int insert(Shield record);

    int insertSelective(Shield record);

    List<Shield> selectByExample(ShieldExample example);

    Shield selectByPrimaryKey(Integer shieldId);

    int updateByExampleSelective(@Param("record") Shield record, @Param("example") ShieldExample example);

    int updateByExample(@Param("record") Shield record, @Param("example") ShieldExample example);

    int updateByPrimaryKeySelective(Shield record);

    int updateByPrimaryKey(Shield record);
}