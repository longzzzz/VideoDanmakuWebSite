package cn.hongpeiming.videodanmakusite.mapper;

import cn.hongpeiming.videodanmakusite.pojo.Complainttype;
import cn.hongpeiming.videodanmakusite.pojo.ComplainttypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ComplainttypeMapper {
    long countByExample(ComplainttypeExample example);

    int deleteByExample(ComplainttypeExample example);

    int deleteByPrimaryKey(Integer complainttypeId);

    int insert(Complainttype record);

    int insertSelective(Complainttype record);

    List<Complainttype> selectByExample(ComplainttypeExample example);

    Complainttype selectByPrimaryKey(Integer complainttypeId);

    int updateByExampleSelective(@Param("record") Complainttype record, @Param("example") ComplainttypeExample example);

    int updateByExample(@Param("record") Complainttype record, @Param("example") ComplainttypeExample example);

    int updateByPrimaryKeySelective(Complainttype record);

    int updateByPrimaryKey(Complainttype record);

    List<Complainttype> selectAll();
}