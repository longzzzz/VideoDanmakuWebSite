package cn.hongpeiming.videodanmakusite.mapper;

import cn.hongpeiming.videodanmakusite.pojo.Complaint;
import cn.hongpeiming.videodanmakusite.pojo.ComplaintExample;
import java.util.List;
import java.util.Map;

import cn.hongpeiming.videodanmakusite.pojo.ComplaintList;
import org.apache.ibatis.annotations.Param;

public interface ComplaintMapper {
    long countByExample(ComplaintExample example);

    int deleteByExample(ComplaintExample example);

    int deleteByPrimaryKey(Integer complaintId);

    int insert(Complaint record);

    int insertSelective(Complaint record);

    List<Complaint> selectByExample(ComplaintExample example);

    Complaint selectByPrimaryKey(Integer complaintId);

    int updateByExampleSelective(@Param("record") Complaint record, @Param("example") ComplaintExample example);

    int updateByExample(@Param("record") Complaint record, @Param("example") ComplaintExample example);

    int updateByPrimaryKeySelective(Complaint record);

    int updateByPrimaryKey(Complaint record);

    List<Complaint> findThisVideoComplaint(Integer VideoId);

    List<Complaint> findPassComplaint(Integer VideoId);

    List<Complaint> findUnionComplaint(Integer userId);

    List<ComplaintList> findDanmakuComplaintNumber();

    List<ComplaintList> findDanmakuPassComplaintNumber();

    void updataComplaintStatus(Integer status,Integer danmakuId);

}