package cn.hongpeiming.videodanmakusite.mapper;

import cn.hongpeiming.videodanmakusite.pojo.BanKeyword;
import cn.hongpeiming.videodanmakusite.pojo.BanKeywordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BanKeywordMapper {
    long countByExample(BanKeywordExample example);

    int deleteByExample(BanKeywordExample example);

    int deleteByPrimaryKey(Integer banKeywordId);

    int insert(BanKeyword record);

    int insertSelective(BanKeyword record);

    List<BanKeyword> selectByExample(BanKeywordExample example);

    BanKeyword selectByPrimaryKey(Integer banKeywordId);

    int updateByExampleSelective(@Param("record") BanKeyword record, @Param("example") BanKeywordExample example);

    int updateByExample(@Param("record") BanKeyword record, @Param("example") BanKeywordExample example);

    int updateByPrimaryKeySelective(BanKeyword record);

    int updateByPrimaryKey(BanKeyword record);
}