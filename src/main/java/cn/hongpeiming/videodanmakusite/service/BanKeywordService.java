package cn.hongpeiming.videodanmakusite.service;

import cn.hongpeiming.videodanmakusite.pojo.BanKeyword;

import java.util.List;

public interface BanKeywordService {

    //查询所有我的屏蔽关键词
    List<BanKeyword> findMyBankeyword(Integer userId);

    //删除关键词
    void removeBankeyword(Integer banKeywordId);

    //添加关键词
    int addBanKeyword(String banKeywordString,Integer userId);

    //重复检测
    boolean repeatBanKeyword(String banKeywordString,Integer userId);

    //查询关键词主键
    int findKeywordKey(String banKeywordString,Integer userId);
}
