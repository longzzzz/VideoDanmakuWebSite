package cn.hongpeiming.videodanmakusite.service;

import cn.hongpeiming.videodanmakusite.pojo.Complaint;
import cn.hongpeiming.videodanmakusite.pojo.ComplaintList;

import java.util.List;

public interface ComplaintService {
    //添加举报
    int addComplaint(Complaint complaint);

    //处理举报
    void checkTheComlaint(Integer complaintId);

    //查询举报
    Complaint findTheComplaintById(Integer complaintId);

    //查询所有
    List<Complaint> findallNotPassComlaint();

    //查询我的举报
    List<Complaint> findMyComplaint(Integer userId);

    //撤销举报
    void removeComplaint(Integer complaintId);

    //重复举报检测
    boolean isComplainted(Integer complaintDanmakuId,Integer complaintUserId);


    //根据视频ID查询所有举报
    List<Complaint> findThisVideoComplaint(Integer VideoId);

    //根据视频ID查询已被通过的举报
    List<Complaint> findPassComplaint(Integer VideoId);

    //查询相同弹幕某个状态的举报
    List<Complaint> findComplaintByDanmakuIdAndStatus(Integer danmakuId,Integer status);

    //处理相同弹幕的未处理的举报
    List<ComplaintList> getUnpassComplaintList();

    //处理相同弹幕的已处理的举报
    List<ComplaintList> getpassComplaintList();

    //为相同弹幕的举报设置状态
    void setComplaintGroupStatus(Integer danmakuId,Integer status);

}
