package cn.hongpeiming.videodanmakusite.service;

import cn.hongpeiming.videodanmakusite.pojo.Shield;

import java.util.List;

public interface ShieldService {
    //添加屏蔽信息
    int addShieldDanmaku(int danmakuId,int UserId);

    //获取登录用户浏览浏览视频的屏蔽信息
    //数据库未涉及VideoId

    //获取用户所有的屏蔽信息
    List<Shield> getVideoShieldDanmaku(int userId);

    //删除这条屏蔽信息
    int DeleteShieldDanmaku(int danmakuId,int LoginUserId);
}
