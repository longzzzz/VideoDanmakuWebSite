package cn.hongpeiming.videodanmakusite.service;

import cn.hongpeiming.videodanmakusite.pojo.Complainttype;

import java.util.List;

public interface ComplainttypeService {
    //获取所有
    List<Complainttype> getAllComplainttype();

    //按id查询
    Complainttype findComplainttypeById(Integer complainttypeId);

    //插入
    boolean addNewComplainttype(String complainttypeName);

    //修改
    boolean editcomplainttype(String complainttypeName);


}
