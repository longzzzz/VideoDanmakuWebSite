package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.ComplaintMapper;
import cn.hongpeiming.videodanmakusite.mapper.ComplainttypeMapper;
import cn.hongpeiming.videodanmakusite.pojo.Complaint;
import cn.hongpeiming.videodanmakusite.pojo.ComplaintExample;
import cn.hongpeiming.videodanmakusite.pojo.ComplaintList;
import cn.hongpeiming.videodanmakusite.service.ComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplaintServiceImpl implements ComplaintService {
    @Autowired
    ComplaintMapper complaintMapper;

    @Autowired
    ComplainttypeMapper complainttypeMapper;

    @Override
    public int addComplaint(Complaint complaint) {
        return complaintMapper.insert(complaint);
    }

    @Override
    public void checkTheComlaint(Integer complaintId) {
        Complaint complaint = complaintMapper.selectByPrimaryKey(complaintId);
        if (complaint.getComplaintStatus()==0)
            complaint.setComplaintStatus(1);
        else
            complaint.setComplaintStatus(0);
        complaintMapper.updateByPrimaryKey(complaint);
    }

    @Override
    public Complaint findTheComplaintById(Integer complaintId) {
        return complaintMapper.selectByPrimaryKey(complaintId);
    }

    @Override
    public List<Complaint> findallNotPassComlaint() {
        return null;
    }

    @Override
    public List<Complaint> findMyComplaint(Integer userId) {
        return complaintMapper.findUnionComplaint(userId);
    }

    @Override
    public void removeComplaint(Integer complaintId) {
        complaintMapper.deleteByPrimaryKey(complaintId);
    }

    @Override
    public boolean isComplainted(Integer complaintDanmakuId, Integer complaintUserId) {
        ComplaintExample example = new ComplaintExample();
        ComplaintExample.Criteria criteria = example.createCriteria();
        criteria.andComplaintUserIdEqualTo(complaintUserId);
        criteria.andComplaintDanmakuIdEqualTo(complaintDanmakuId);
        if (complaintMapper.countByExample(example)==0)
            return false;
        else
            return true;
    }

    @Override
    public List<Complaint> findThisVideoComplaint(Integer VideoId) {
        return complaintMapper.findThisVideoComplaint(VideoId);
    }

    @Override
    public List<Complaint> findPassComplaint(Integer VideoId) {
        return complaintMapper.findPassComplaint(VideoId);
    }

    @Override
    public List<Complaint> findComplaintByDanmakuIdAndStatus(Integer danmakuId, Integer status) {
        ComplaintExample example = new ComplaintExample();
        ComplaintExample.Criteria criteria = example.createCriteria();
        criteria.andComplaintDanmakuIdEqualTo(danmakuId);
        criteria.andComplaintStatusEqualTo(status);
        List<Complaint>  complaints = complaintMapper.selectByExample(example);
        for (Complaint complaint:complaints)
        {
            complaint.setComplainttype(complainttypeMapper.selectByPrimaryKey(complaint.getComplaintType()));
        }
        return complaints;
    }

    @Override
    public List<ComplaintList> getUnpassComplaintList() {
        List<ComplaintList> complaintLists = complaintMapper.findDanmakuComplaintNumber();
        return complaintLists;
    }

    @Override
    public List<ComplaintList> getpassComplaintList() {
        List<ComplaintList> complaintLists = complaintMapper.findDanmakuPassComplaintNumber();
        return complaintLists;
    }

    @Override
    public void setComplaintGroupStatus(Integer danmakuId, Integer status) {
        complaintMapper.updataComplaintStatus(status,danmakuId);
    }
}
