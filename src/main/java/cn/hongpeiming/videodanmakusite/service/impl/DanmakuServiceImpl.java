package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.DanmakuMapper;
import cn.hongpeiming.videodanmakusite.pojo.Danmaku;
import cn.hongpeiming.videodanmakusite.pojo.DanmakuExample;
import cn.hongpeiming.videodanmakusite.pojo.Shield;
import cn.hongpeiming.videodanmakusite.service.DanmakuService;
import cn.hongpeiming.videodanmakusite.utils.PageRequest;
import cn.hongpeiming.videodanmakusite.utils.PageResult;
import cn.hongpeiming.videodanmakusite.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DanmakuServiceImpl implements DanmakuService {
    @Autowired
    DanmakuMapper danmakuMapper;

    @Override
    public List<Danmaku> getAllDanmakuByVideoId(Integer videoId) {
        DanmakuExample example = new DanmakuExample();
        DanmakuExample.Criteria criteria = example.createCriteria();
        criteria.andDanmakuVideoidEqualTo(videoId);
        example.setOrderByClause("danmaku_videotime ASC");
        List<Danmaku> danmakus  = danmakuMapper.selectByExample(example);
        return danmakus;
    }

    @Override
    public List<Danmaku> getAllDanmakuByVideoIdWithPage(Integer videoId) {
        return null;
    }

    @Override
    public List<Danmaku> getUnBanDanmakuByVideoId(Integer videoId) {
        DanmakuExample example = new DanmakuExample();
        example.setOrderByClause("danmaku_videotime ASC");
        DanmakuExample.Criteria criteria = example.createCriteria();
        criteria.andDanmakuVideoidEqualTo(videoId);
        criteria.andDanmakuStatusEqualTo(0);
        List<Danmaku> danmakus  = danmakuMapper.selectByExample(example);
        return danmakus;
    }

    @Override
    public Danmaku getDanmakuById(Integer danmakuId) {
        Danmaku danmaku = danmakuMapper.selectByPrimaryKey(danmakuId);
        return danmaku;
    }

    @Override
    public boolean sendDanmaku(Danmaku danmaku) {
        try {
            danmakuMapper.insert(danmaku);
        }catch (Exception e)
        {
            return false;
        }
        return true;
    }

    @Override
    public boolean banTheDanmaku(Integer danmakuId) {
        try {
            //提交到管理员的举报信息
        }catch (Exception e)
        {
            return false;
        }
        return true;
    }

    @Override
    public boolean switchTheDanmakuStatus(Integer danmakuId,Integer danmakuStatus) {
        try {
            Danmaku danmaku = danmakuMapper.selectByPrimaryKey(danmakuId);
            danmaku.setDanmakuStatus(danmakuStatus);
            danmakuMapper.updateByPrimaryKeySelective(danmaku);
        }catch (Exception e)
        {
            return false;
        }

        return true;
    }

    @Override
    public boolean checkDanmakuStatus(Integer danmakuId) {
        Danmaku danmaku = danmakuMapper.selectByPrimaryKey(danmakuId);
        if (danmaku.getDanmakuStatus()==0)
            return false;
        return true;
    }

    @Override
    public List<Danmaku> getUnShieldDanmaku(List<Danmaku> danmakus, List<Shield> shields) {
        List<Danmaku> s_danmakus = new ArrayList<Danmaku>();
        //System.out.println("查询屏蔽弹幕 danmakus.size():"+danmakus.size());
        for (Danmaku danmaku : danmakus)
        {
            boolean b = true;
            for (Shield shield : shields)
            {
                //System.out.println("对比:  "+danmaku.getDanmakuId()+"  "+shield.getShieldBanid());
                if (danmaku.getDanmakuId().equals(shield.getShieldBanid()))
                {
                    //System.out.println("相同");
                    b = false;
                }
            }
            if (b)
            {
                s_danmakus.add(danmaku);
            }
        }
        return s_danmakus;
    }

    @Override
    public PageResult findPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    private PageInfo<Danmaku> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Danmaku> danmakus = danmakuMapper.selectPage(1);
        return new PageInfo<Danmaku>(danmakus);
    }

}
