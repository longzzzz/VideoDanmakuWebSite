package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.UserMapper;
import cn.hongpeiming.videodanmakusite.mapper.VideoMapper;
import cn.hongpeiming.videodanmakusite.mapper.VideotypeMapper;
import cn.hongpeiming.videodanmakusite.pojo.Video;
import cn.hongpeiming.videodanmakusite.pojo.VideoExample;
import cn.hongpeiming.videodanmakusite.pojo.Videotype;
import cn.hongpeiming.videodanmakusite.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl  implements VideoService {
    @Autowired
    VideoMapper videoMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    VideotypeMapper videotypeMapper;

    @Override
    public List<Video> selectAllVideo() {

        return videoMapper.UnionselectAllVideo();
    }

    @Override
    public Video selectVideoById(Integer Videoid) {
        Video video = videoMapper.selectByPrimaryKey(Videoid);
        video.setUser(userMapper.selectByPrimaryKey(video.getVideoAuthorid()));
        video.setVideotypepojo(videotypeMapper.selectByPrimaryKey(video.getVideoType()));
        return video;
    }

    @Override
    public List<Video> selectMyVideo(Integer userId) {
        VideoExample example = new VideoExample();
        VideoExample.Criteria criteria = example.createCriteria();
        criteria.andVideoAuthoridEqualTo(userId);
        return videoMapper.selectByExample(example);
    }
}
