package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.BanKeywordMapper;
import cn.hongpeiming.videodanmakusite.pojo.BanKeyword;
import cn.hongpeiming.videodanmakusite.pojo.BanKeywordExample;
import cn.hongpeiming.videodanmakusite.service.BanKeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BanKeywordServiceImpl implements BanKeywordService {
    @Autowired
    BanKeywordMapper banKeywordMapper;

    @Override
    public List<BanKeyword> findMyBankeyword(Integer userId) {
        BanKeywordExample example = new BanKeywordExample();
        BanKeywordExample.Criteria criteria = example.createCriteria();
        criteria.andBanKeywordUseridEqualTo(userId);
        return banKeywordMapper.selectByExample(example);
    }

    @Override
    public void removeBankeyword(Integer banKeywordId) {
        banKeywordMapper.deleteByPrimaryKey(banKeywordId);
    }

    @Override
    public int addBanKeyword(String banKeywordString, Integer userId) {
        BanKeyword banKeyword = new BanKeyword();
        banKeyword.setBanKeywordString(banKeywordString);
        banKeyword.setBanKeywordUserid(userId);
        return banKeywordMapper.insert(banKeyword);
    }

    @Override
    public boolean repeatBanKeyword(String banKeywordString, Integer userId) {
        BanKeywordExample example = new BanKeywordExample();
        BanKeywordExample.Criteria criteria = example.createCriteria();
        criteria.andBanKeywordStringEqualTo(banKeywordString);
        criteria.andBanKeywordUseridEqualTo(userId);
        if (banKeywordMapper.countByExample(example)==0)
            return false;
        else
            return true;
    }

    @Override
    public int findKeywordKey(String banKeywordString, Integer userId) {
        BanKeywordExample example = new BanKeywordExample();
        BanKeywordExample.Criteria criteria = example.createCriteria();
        criteria.andBanKeywordStringEqualTo(banKeywordString);
        criteria.andBanKeywordUseridEqualTo(userId);
        try {
            BanKeyword banKeyword = banKeywordMapper.selectByExample(example).get(0);
            return banKeyword.getBanKeywordId();
        }catch (Exception e)
        {
            System.out.println(e);
            return 0;
        }
    }
}
