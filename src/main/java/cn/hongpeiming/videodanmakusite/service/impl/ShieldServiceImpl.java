package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.ShieldMapper;
import cn.hongpeiming.videodanmakusite.pojo.DanmakuExample;
import cn.hongpeiming.videodanmakusite.pojo.Shield;
import cn.hongpeiming.videodanmakusite.pojo.ShieldExample;
import cn.hongpeiming.videodanmakusite.service.ShieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShieldServiceImpl implements ShieldService {
    @Autowired
    ShieldMapper shieldMapper;

    @Override
    public int addShieldDanmaku(int danmakuId,int UserId) {
        Shield shield = new Shield();
        shield.setShieldUserid(UserId);
        shield.setShieldBanid(danmakuId);
        shieldMapper.insert(shield);
        return 1;
    }

    @Override
    public List<Shield> getVideoShieldDanmaku(int userId) {
        ShieldExample example = new ShieldExample();
        ShieldExample.Criteria criteria = example.createCriteria();
        criteria.andShieldUseridEqualTo(userId);
        List<Shield> shields = shieldMapper.selectByExample(example);
        return shields;
    }

    @Override
    public int DeleteShieldDanmaku(int danmakuId,int LoginUserId) {
        ShieldExample example = new ShieldExample();
        ShieldExample.Criteria criteria = example.createCriteria();
        criteria.andShieldUseridEqualTo(LoginUserId);
        criteria.andShieldBanidEqualTo(danmakuId);
        shieldMapper.deleteByExample(example);
        return 0;
    }
}
