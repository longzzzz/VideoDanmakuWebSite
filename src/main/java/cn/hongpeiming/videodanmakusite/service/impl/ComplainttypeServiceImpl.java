package cn.hongpeiming.videodanmakusite.service.impl;

import cn.hongpeiming.videodanmakusite.mapper.ComplainttypeMapper;
import cn.hongpeiming.videodanmakusite.pojo.Complainttype;
import cn.hongpeiming.videodanmakusite.service.ComplainttypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplainttypeServiceImpl implements ComplainttypeService {
    @Autowired
    ComplainttypeMapper complainttypeMapper;

    @Override
    public List<Complainttype> getAllComplainttype() {
        List<Complainttype> complainttypes = complainttypeMapper.selectAll();
        return complainttypes;
    }

    @Override
    public Complainttype findComplainttypeById(Integer complainttypeId) {
        return null;
    }

    @Override
    public boolean addNewComplainttype(String complainttypeName) {
        return false;
    }

    @Override
    public boolean editcomplainttype(String complainttypeName) {
        return false;
    }
}
