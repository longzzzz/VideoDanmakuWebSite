package cn.hongpeiming.videodanmakusite.controller;

import cn.hongpeiming.videodanmakusite.mapper.UserMapper;
import cn.hongpeiming.videodanmakusite.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class testController {
//
//    @Autowired
//    private UserService userService;

    @Autowired
    UserMapper userMapper;



    @RequestMapping("test")
    public String test(Model model)
    {

        model.addAttribute("test","rua!");
//        User user = userService.getUserById(1);
//        model.addAttribute("user",user);
        User user = userMapper.selectByPrimaryKey(1);
        model.addAttribute("user",user);
        return "test";
    }


}
