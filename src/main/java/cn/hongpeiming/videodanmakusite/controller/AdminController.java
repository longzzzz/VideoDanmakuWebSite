package cn.hongpeiming.videodanmakusite.controller;

import cn.hongpeiming.videodanmakusite.pojo.User;
import cn.hongpeiming.videodanmakusite.pojo.Video;
import cn.hongpeiming.videodanmakusite.service.UserService;
import cn.hongpeiming.videodanmakusite.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    VideoService videoService;

    @Autowired
    UserService userService;

    //personal
    //侧边栏
    @RequestMapping("adminController/{type}")
    public String admin(@PathVariable("type")String type, Model model)
    {
        try {
            model.addAttribute("type",type);
        }catch (Exception e)
        {
            model.addAttribute("type","main");
        }

        return "admin/admin";
    }

    @RequestMapping("adminController/AdminDanmakuManager/{videoId}")
    public String AdminDanmakuManager(@PathVariable("videoId") String videoId,HttpSession httpSession, Model model)
    {
        try {
            model.addAttribute("type","AdminDanmakuManager");
        }catch (Exception e)
        {
            model.addAttribute("type","main");
        }

        model.addAttribute("videoId",videoId);

        return "admin/admin";
    }

    @RequestMapping("adminControllerRight/ComplaintManagerPage")
    public String adminComplaintManager(@RequestParam("type") String type,HttpSession httpSession, Model model)
    {
        if (httpSession.getAttribute("LoginUserId")==null)
        {
            model.addAttribute("status","littleWindows");
            return "LoginAndRegister";
        }

        if (type!=null)
            model.addAttribute("type",type);
        else
            model.addAttribute("type","unpass");

        return "admin/ComplaintManager";
    }

    @RequestMapping("adminControllerRight/VideoDanmakuManager")
    public String adminVideoDanmakuManager(HttpSession httpSession, Model model)
    {
        if (httpSession.getAttribute("LoginUserId")==null)
        {
            model.addAttribute("status","littleWindows");
            return "LoginAndRegister";
        }
        List<Video> videoList = videoService.selectAllVideo();
        model.addAttribute("videoList",videoList);

        return "admin/include_admin_myvideo";
    }


    @RequestMapping("adminControllerRight/AdminDanmakuManager/{videoId}")
    public String adminDanmakuManager(@PathVariable("videoId") String videoId,HttpSession httpSession, Model model)
    {
        if (httpSession.getAttribute("LoginUserId")==null)
        {
            model.addAttribute("status","littleWindows");
            return "LoginAndRegister";
        }
        try {
            User user = userService.selectUserByUserId((Integer) httpSession.getAttribute("LoginUserId"));
            if (user.getUserType()!=0)
            {
                System.out.println("权限不足");
                return "LoginAndRegister";
            }

        }catch (Exception e)
        {
            System.out.println(e);
            return "LoginAndRegister";
        }
        model.addAttribute("videoId",videoId);

        return "admin/include_admin_danmakumanager";
    }

}
