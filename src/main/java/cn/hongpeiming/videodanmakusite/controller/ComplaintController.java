package cn.hongpeiming.videodanmakusite.controller;

import cn.hongpeiming.videodanmakusite.pojo.Complaint;
import cn.hongpeiming.videodanmakusite.pojo.ComplaintList;
import cn.hongpeiming.videodanmakusite.service.ComplaintService;
import cn.hongpeiming.videodanmakusite.service.ComplainttypeService;
import cn.hongpeiming.videodanmakusite.service.DanmakuService;
import cn.hongpeiming.videodanmakusite.service.VideoService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ComplaintController {
    @Autowired
    DanmakuService danmakuService;

    @Autowired
    ComplaintService complaintService;

    @Autowired
    ComplainttypeService complainttypeService;

    @Autowired
    VideoService videoService;



    //查询未被处理的举报合集
    @RequestMapping("/Admin/ComplaintManager/{type}")
    @ResponseBody
    public String ComplaintManager(@PathVariable("type")String type)
    {
        //权限检测


        //条件查询
        //条件分为 未处理  已处理
        List<ComplaintList> complaintLists = new ArrayList<>();
        Integer Status = 0;
        switch (type)
        {
            case "unpass":
                complaintLists = complaintService.getUnpassComplaintList();
                break;
            case "pass":
                complaintLists = complaintService.getpassComplaintList();
                Status = 1;
                break;
            default:
                return "NotData";
        }
        //更新信息
        for (ComplaintList complaintList:complaintLists)
        {
            complaintList.setDanmaku(danmakuService.getDanmakuById(complaintList.getDanmakuId()));
            complaintList.setVideo(videoService.selectVideoById(complaintList.getDanmaku().getDanmakuVideoid()));
            //complaintList.setComplaints(complaintService.findComplaintByDanmakuIdAndStatus(complaintList.getDanmakuId(),Status));
        }

        String JSONcomplaintLists = JSON.toJSONString(complaintLists);
        return JSONcomplaintLists;
    }

    //查询未被处理的举报合集
    @RequestMapping("/Admin/ComplaintList/{danmakuId}")
    @ResponseBody
    public String SingleComplaintManager(@PathVariable("danmakuId")Integer danmakuId)
    {
            ComplaintList complaintList = new ComplaintList();
            complaintList.setDanmakuId(danmakuId);
            complaintList.setDanmaku(danmakuService.getDanmakuById(complaintList.getDanmakuId()));
            complaintList.setVideo(videoService.selectVideoById(complaintList.getDanmaku().getDanmakuVideoid()));
            complaintList.setComplaints(complaintService.findComplaintByDanmakuIdAndStatus(complaintList.getDanmakuId(),0));
            String JsonComplaintList = JSON.toJSONString(complaintList);
        return JsonComplaintList;
    }
    //查询未被处理的举报合集
    @RequestMapping("/Admin/ComplaintListPage/{danmakuId}")
    public String SingleComplaintManagerPage(@PathVariable("danmakuId")Integer danmakuId, Model model)
    {
            ComplaintList complaintList = new ComplaintList();
            complaintList.setDanmakuId(danmakuId);
            complaintList.setDanmaku(danmakuService.getDanmakuById(complaintList.getDanmakuId()));
            complaintList.setVideo(videoService.selectVideoById(complaintList.getDanmaku().getDanmakuVideoid()));
            complaintList.setComplaints(complaintService.findComplaintByDanmakuIdAndStatus(complaintList.getDanmakuId(),0));
            //String JsonComplaintList = JSON.toJSONString(complaintList);
        model.addAttribute("complaintList",complaintList);
        return "/admin/SingleComplaintPage";
    }
    //查询被处理的举报合集
    @RequestMapping("/Admin/PassedComplaintListPage/{danmakuId}")
    public String PassedSingleComplaintManagerPage(@PathVariable("danmakuId")Integer danmakuId, Model model)
    {
            ComplaintList complaintList = new ComplaintList();
            complaintList.setDanmakuId(danmakuId);
            complaintList.setDanmaku(danmakuService.getDanmakuById(complaintList.getDanmakuId()));
            complaintList.setVideo(videoService.selectVideoById(complaintList.getDanmaku().getDanmakuVideoid()));
            complaintList.setComplaints(complaintService.findComplaintByDanmakuIdAndStatus(complaintList.getDanmakuId(),2));
            //String JsonComplaintList = JSON.toJSONString(complaintList);
        model.addAttribute("complaintList",complaintList);
        return "/admin/PassedSingleComplaintPage";
    }



    //处理当条弹幕的所有举报
    @RequestMapping("/Admin/PassComplaint/{danmakuId}/{status}")
    @ResponseBody
    public String PassComplaint(@PathVariable("danmakuId")Integer danmakuId,@PathVariable("status")Integer status)
    {
        //传入参数status 2 通过举报
        //弹幕状态 2 违规
        //举报状态 0 未处理 1 驳回 2 通过
        if (status==2)
        {
            danmakuService.switchTheDanmakuStatus(danmakuId,2);
            complaintService.setComplaintGroupStatus(danmakuId,2);
        }
        else if(status==1)
        {
            danmakuService.switchTheDanmakuStatus(danmakuId,0);
            complaintService.setComplaintGroupStatus(danmakuId,1);
        }

        return "";
    }




    @RequestMapping("/testData")
    @ResponseBody
    public List<ComplaintList> testData()
    {
        return complaintService.getUnpassComplaintList();
    }

}
